![logo](https://gitlab.com/esequeira/ci-demo-esequeira/-/raw/master/images/logo.png)

# spring-boot-demo

Para ejecutar el servidor

```bash
mvn string-boot:run
```

Para compilar

```bash
mvn package && java -jar ./target/myproject-0.0.1-SNAPSHOT.jar 
```

En docker

```bash
sudo docker build -t demo:1.10 .
sudo docker run --network host -p 8080:8080 demo:1.10
```

Acceder por browser a (http://localhost:8080/)
